//
//  ViewController.h
//  publicGeneralize
//
//  Created by admin on 2018/9/23.
//  Copyright © 2018年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - 尺寸相关
#define QYScreenW [UIScreen mainScreen].bounds.size.width
#define QYScreenH [UIScreen mainScreen].bounds.size.height
#define QYWeakSelf(weakSelf)  __weak __typeof(&*self)weakSelf = self;  // 弱引用
#ifdef DEBUG // 开发
#define QYLog(...) NSLog(@"%s %d \n%@\n\n",__func__,__LINE__,[NSString stringWithFormat:__VA_ARGS__])
#else // 生产
#define QYLog(...) //NSLog(@"%s %d \n%@\n\n",__func__,__LINE__,[NSString stringWithFormat:__VA_ARGS__])
#endif

@interface ViewController : UIViewController


@property (nonatomic ,strong)NSURL *theUrl;

@end

