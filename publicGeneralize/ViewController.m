//
//  ViewController.m
//  publicGeneralize
//
//  Created by admin on 2018/9/23.
//  Copyright © 2018年 admin. All rights reserved.
//

#import "ViewController.h"
#import "QYWebView.h"
#import "QYAddTheWebViewUse.h"
#import <WebKit/WebKit.h>
@interface ViewController ()<WKNavigationDelegate>

@property(nonatomic, strong) WKWebView *webView;

@property(nonatomic, strong) UIView *mainView;

@property(nonatomic, strong) UIProgressView *progressView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //NSString *urlStr = @"https://www.baidu.com";
    
    self.title = @"兼职网赚";
    
     NSString *urlStr =@"http://682050.com";//展示微信公众号的页面
     
     NSURL *url =[NSURL URLWithString:urlStr];
     
     WKWebView *webView1 = [[WKWebView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
     
     [webView1 loadRequest:[NSURLRequest requestWithURL:url]];
    
     webView1.backgroundColor = [UIColor whiteColor];
     
     [self.view addSubview:webView1];
    
    /*
    [self addMainView];
    [self creatWebView];
    self.webView.backgroundColor = [UIColor whiteColor];
    
    static NSString  *strUrl = @"http://682050.com";
    
    self.theUrl = [NSURL URLWithString:strUrl];
    */

}


- (void)addMainView {
    QYWeakSelf(weakSelf)
    UIView *mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QYScreenW, QYScreenH)];
    mainView.backgroundColor = [UIColor clearColor];
    _mainView = mainView;
    [weakSelf.view addSubview:mainView];
    UIProgressView *progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 64, QYScreenW, 1)];
    progressView.progress = 0;
    _progressView = progressView;
    [weakSelf.view addSubview:progressView];
}

- (void)creatWebView {
    QYWeakSelf(weakSelf)
    _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, QYScreenW, QYScreenH)];
    _webView.backgroundColor = [UIColor whiteColor];
    _webView.navigationDelegate = weakSelf;
    _webView.scrollView.bounces = NO;
    [weakSelf.mainView addSubview:_webView];
    
    // 添加观察者
    [_webView addObserver:weakSelf forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:NULL]; // 进度
    [_webView addObserver:weakSelf forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL]; // 标题
}

//个函数时在赋值的时候调用，例如：self.theUrl = 的时候
- (void)setTheUrl:(NSURL *)theUrl
{
    _theUrl = theUrl;
    QYWeakSelf(weakSelf)
    NSURLRequest *request = [NSURLRequest requestWithURL:weakSelf.theUrl];
    [_webView loadRequest:request];

}

#pragma maker ====================添加的观察者代理===========================

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    QYWeakSelf(weakSelf)
    if ([keyPath isEqualToString:@"estimatedProgress"])
    {
        if (object == _webView)
        {
            [weakSelf.progressView setAlpha:1.0f];
            [weakSelf.progressView setProgress:weakSelf.webView.estimatedProgress animated:YES];
            if(weakSelf.webView.estimatedProgress >= 1.0f)
            {
                [UIView animateWithDuration:0.3 delay:0.3 options:UIViewAnimationOptionCurveEaseOut animations:^{
                    [weakSelf.progressView setAlpha:0.0f];
                } completion:^(BOOL finished) {
                    [weakSelf.progressView setProgress:0.0f animated:NO];
                }];
            }
        }else
        {
           [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        }
    }else if ([keyPath isEqualToString:@"title"])
    {
        QYLog(@"%@",weakSelf.webView.title);
        if (object == weakSelf.webView)
        {
                weakSelf.title = weakSelf.webView.title;
        }else
        {
                [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        }
    }else
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
    
}

#pragma maker =======================web进行中=============================
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    QYLog(@"调用失败");
}
// 页面加载完毕时调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation
{
    QYLog(@"成功");
}



#pragma maker ===================最后进行===============================

// 当对象即将销毁的时候调用
- (void)dealloc {
    NSLog(@"webView释放");
    [_webView removeObserver:self forKeyPath:@"estimatedProgress"];
    [_webView removeObserver:self forKeyPath:@"title"];
    _webView.navigationDelegate = nil;
}

@end
