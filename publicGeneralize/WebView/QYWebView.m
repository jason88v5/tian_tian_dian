//
//  QYWebView.m
//  part_time
//
//  Created by admin on 2018/9/23.
//  Copyright © 2018年 admin. All rights reserved.
//

#import "QYWebView.h"
#import <WebKit/WebKit.h>

@interface QYWebView ()<WKNavigationDelegate>
/** <#注释#> */
@property(nonatomic, strong) WKWebView *webView;
/** 注释 */
@property(nonatomic, strong) UIView *mainView;
/** <#注释#> */
@property(nonatomic, strong) UIProgressView *progressView;

@end

@implementation QYWebView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addMainView];
    [self creatWebView];
    self.view.backgroundColor = [UIColor whiteColor];
    
    
}


#pragma mark - 添加进度条
- (void)addMainView {
    QYWeakSelf(weakSelf)
    UIView *mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, QYScreenW, QYScreenH)];
    mainView.backgroundColor = [UIColor clearColor];
    _mainView = mainView;
    [weakSelf.view addSubview:mainView];
    UIProgressView *progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 64, QYScreenW, 1)];
    progressView.progress = 0;
    _progressView = progressView;
    [weakSelf.view addSubview:progressView];
}
- (void)creatWebView {
    QYWeakSelf(weakSelf)
    _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, QYScreenW, QYScreenH)];
    _webView.backgroundColor = [UIColor whiteColor];
    _webView.navigationDelegate = weakSelf;
    _webView.scrollView.bounces = NO;
    [weakSelf.mainView addSubview:_webView];
    // 添加观察者
    [_webView addObserver:weakSelf forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:NULL]; // 进度
    [_webView addObserver:weakSelf forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL]; // 标题
}

//个函数时在赋值的时候调用，例如：self.theUrl = 的时候
- (void)setTheUrl:(NSURL *)theUrl
{
    _theUrl = theUrl;
    QYWeakSelf(weakSelf)
    NSURLRequest *request = [NSURLRequest requestWithURL:weakSelf.theUrl];
    [_webView loadRequest:request];
    
}

/*错误代码,因为这个set方法和调用的set名字不同，所以一直调用不了
- (void)setUrl:(NSURL *)url {
    _theUrl = url;
    QYWeakSelf(weakSelf)
    NSURLRequest *request = [NSURLRequest requestWithURL:weakSelf.theUrl];
    [_webView loadRequest:request];
}
 
 */


// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    
}
// 页面加载完毕时调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
    
}
#pragma mark - 监听加载进度
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    QYWeakSelf(weakSelf)
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        if (object == _webView) {
            [weakSelf.progressView setAlpha:1.0f];
            [weakSelf.progressView setProgress:weakSelf.webView.estimatedProgress animated:YES];
            if(weakSelf.webView.estimatedProgress >= 1.0f) {
                [UIView animateWithDuration:0.3 delay:0.3 options:UIViewAnimationOptionCurveEaseOut animations:^{
                    [weakSelf.progressView setAlpha:0.0f];
                } completion:^(BOOL finished) {
                    [weakSelf.progressView setProgress:0.0f animated:NO];
                }];
            }
        } else {
            [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        }
    }  else if ([keyPath isEqualToString:@"title"]) {
        QYLog(@"%@",weakSelf.webView.title);
        if (object == weakSelf.webView) {
            weakSelf.title = weakSelf.webView.title;
        } else {
            [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}
// 当对象即将销毁的时候调用
- (void)dealloc {
    NSLog(@"webView释放");
    [_webView removeObserver:self forKeyPath:@"estimatedProgress"];
    [_webView removeObserver:self forKeyPath:@"title"];
    _webView.navigationDelegate = nil;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
